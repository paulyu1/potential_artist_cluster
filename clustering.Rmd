---
title: "clustering"
author: "Paul Yu"
date: "5/21/2021"
output: html_document
---
#package
```{r}
library(tidyverse)
library(rlist)
`%notin%` <- Negate(`%in%`)
```

# 0. Description and abstract
This file contains the clustering function, loopingAllArtist by clustering function, additional loop based on different topK, useful tool to debug/display artist name, grid search hyperparameter tuning.
The main purpose of this clustering function is to find out a group composed of similar artists in terms of certain selected metric.

Data used are mainly artist embedding/network data.

loop structure:
loop based on diff topK
  loop all the artist
    clustering

# 1. Clustering
## Data Input/Preprocessing
```{r}
yrs=c(17, 18, 19, 20, 21)

# artist info
artist_info = data.frame()
for (yr in yrs){
  df = read.table(file=paste0('dta/network_links/representative_C2_03_info_trimmed_network_20', yr, '-01-01.tsv'), sep="\t", header = TRUE)
  df['yr'] = yr
  artist_info = rbind(artist_info, df) %>%
    distinct()
}

artist_info = artist_info %>%
  mutate(X2 = str_remove(top.C2.artists.comb, X1)) %>%
  mutate(X2 = str_remove(X2, "/"))

# artist name
artist_name = data.frame()
for (yr in yrs){
  df = read_tsv(file='dta/artist_info/artist_core_fan_size_2021-01-01.tsv', col_names = TRUE)  
  artist_name = rbind(artist_name, df)
}
artist_name = artist_name %>% 
  select(-num_core_fan) %>%
  distinct()
```

## Useful tool
1. showX1X2Name_df: show x1 x2 name given dataframe
2. showX1X2Name_vec: show x1 x2 name given vector
3. searchSingleTop_idName: searcg artist with top metric
```{r}
# data used by tool
artist_name_df_info = artist_info %>%
  mutate(X1name = (artist_info %>% left_join(artist_name %>% mutate(clusterID = paste0("a_", artist_id)), 
                                             by=c("X1"="clusterID"))) %>% `$`(artist_name)) %>%
  mutate(X2name = (artist_info %>% left_join(artist_name %>% mutate(clusterID = paste0("a_", artist_id)), 
                                             by=c("X2"="clusterID"))) %>%  `$`(artist_name)) %>%
  distinct()

# tool function 
showX1X2Name_df = function(df){
  #debuging: show x1 x2 name
  df_d = df %>%
    left_join(artist_name_df_info, by=c("X1", "X2")) %>%
    select(-context.free.comb.uu.ratio.x, -context.free.comb.uu.ratio.y)
  return(df_d)
}

showX1X2Name_vec = function(vec){
  #debuging: show x1 x2 name
  artist_name_df_d = artist_name_df %>%
    filter(artist_id %in% (vec %>% str_remove("a_"))) %>%
    select(-num_core_fan)
  return(artist_name_df_d)
}

searchSingleTop_idName = function(id=NULL, name, yr){
  i = yr
  if (!is.null(id)){
    artist_name_df_info %>% 
      filter(yr == i, X1 == id) %>% select(X1, X2, X1name, X2name, context.free.comb.uu.ratio) %>% arrange(-context.free.comb.uu.ratio)
  } else {
    artist_name_df_info %>% 
      filter(r == i, str_detect(X1name, name))  %>% select(X1, X2, X1name, X2name, context.free.comb.uu.ratio) %>% arrange(-context.free.comb.uu.ratio)
  }
}
```

## clustering main function
* arrangement metric can be different from sampling metric
```{r}
randomPickFunc = function(top_n_artist_afterRemoval, samplingMetric="comb.artist.uu.ratio"){
  weighted_sample_artist = sample(top_n_artist_afterRemoval$X2, 1, prob=top_n_artist_afterRemoval[[samplingMetric]])
  return(weighted_sample_artist)
}

GetTopKRelatedArtist = function(artist_info, artists, top_n_conv, arrangeMetric, biTop){
  # top n artist based on arrangement metric 
  top_n_artist = artist_info %>% 
    filter(X1 %in% artists) %>%
    group_by(X1) %>%
    top_n(top_n_conv, (!!as.name(arrangeMetric)))
  #browser()
  # BiTop
  if (biTop == F){
    return(top_n_artist)
  } else {
    top_n_artist = artist_info %>%
      filter(X1 %in% top_n_artist$X2) %>%
      group_by(X1) %>%
      top_n(top_n_conv, (!!as.name(arrangeMetric))) %>%
      ungroup() %>%
      filter(X2 %in% artists) %>%
      rename(
        X1 = X2,
        X2 = X1,
        ) #swap the name, X2 is what we should really focus
    return(top_n_artist)
  }
}

loopingForCluster = function(artists, samegroup_list, selected_artists, artist_info, top_n_conv, randomPick,
                             arrangeMetric, biTop, loopStop, loopStopNum) {
  # purpose: for individual artist, we look for the cluster
  # input: artists(random state only one artist, normal state several), counter in the artist cluster, the list containing same group of artist
  # 
  # This is a recursive function
  # Break condition is when you cannot find more candidate in the cluster
  #
  # Output: artists_groups_list, for Null element, they might be contained in other group.
  #
  
  #browser()
  top_n_artists = GetTopKRelatedArtist(artist_info, artists, top_n_conv=top_n_conv, arrangeMetric=arrangeMetric, biTop=biTop)
  
  # filter out selected artist
  top_n_artists_afterRemoval = top_n_artists %>%
    filter(X2 %in% (top_n_artists$X2 %>% 
      unique() %>%
      setdiff(selected_artists))
    )
  
  # convergence 
  if(nrow(top_n_artists_afterRemoval) == 0){ #if converge then jump out
    samegroup_list = c(samegroup_list, artists)
    selected_artists = c(selected_artists, artists)
    result = list(samegroup_list=samegroup_list, selected_artists=selected_artists)
    return(result)
  } else { # find out new connected artists
    if (randomPick == TRUE){ # Random picking
      next_artists = randomPickFunc(top_n_artists_afterRemoval, samplingMetric=arrangeMetric)
      samegroup_list = c(samegroup_list, artists)
      selected_artists = c(selected_artists, artists)
    } else { # Non-Random Picking
      #browser()
      next_artists = top_n_artists_afterRemoval$X2 %>% unique()
      samegroup_list = c(samegroup_list, artists)
      samegroup_list = samegroup_list %>% unique
      selected_artists = c(selected_artists, artists)
      selected_artists = selected_artists %>% unique
    }
    
    if(loopStop == T){ # false then ignore loopStopNum automatically
      loopStopNum = loopStopNum - 1
      if (loopStopNum == 0){
        result = list(samegroup_list=samegroup_list, selected_artists=selected_artists)
        return(result)
      }
    }
    return(Recall(next_artists, samegroup_list, selected_artists, artist_info, top_n_conv=top_n_conv, randomPick=randomPick,  arrangeMetric=arrangeMetric, biTop=biTop, loopStop=loopStop, loopStopNum=loopStopNum))
      
  }
  return(0)
}
```

## Sample single artist
```{r}
arrangeMetric="context.free.comb.uu.ratio"
selected_artists_test = c()
samegroup_list_test = c()
artist_info_selected = artist_info %>%
  filter(yr == 17) %>%
  select(X1, X2, context.free.comb.uu.ratio) %>%
  arrange(-context.free.comb.uu.ratio)
  
# we could test c("a_2578468", "a_210761", "a_253716", "a_5603802")
result <- loopingForCluster("a_206093", samegroup_list_test, selected_artists_test, artist_info=artist_info_selected, top_n_conv=5, arrangeMetric="context.free.comb.uu.ratio", randomPick=F, biTop=T, loopStop=T, loopStopNum=3)
result[['samegroup_list']] %>% showX1X2Name_vec
searchSingleTop_idName(id='a_206093', yr=17) 
artist_info_selected %>%
      filter(X1 == 'a_5000058') 
```

# 2. Loop through all artist
paremeter set based on grid search hyperparameter tuning
top_n_conv = 3, randomPick = F, biTop = T, loopStop = T, loopStopNum = 3, arrangeMetric = "context.free.comb.uu.ratio"
```{r}
# Parameter Setting
yrs=c(17, 18, 19, 20, 21)
loopAllArtistFunc = function(top_n_conv, randomPick = F, biTop = T, loopStop = T, loopStopNum = 3, arrangeMetric = "context.free.comb.uu.ratio", 
                             artist_info, artistNoCluster){
  concat_output_allArtists = data.frame(artist_id=double(), artist_name=character(), num_core_fan=double(), 
                                        input_artist=character(), yr=double(), stringsAsFactors=FALSE)
  selected_artists_test = c()
  samegroup_list_test = c()
  # Loop
  for (i in yrs){
    #browser()
    # arrange and the sorting in the function will be more time saving
    artist_info_selected = artist_info %>%
      filter(yr == i) %>%
      ungroup() %>%
      select(X1, X2, (!!as.name(arrangeMetric))) %>%
      arrange(-(!!as.name(arrangeMetric)))
      
    
    # findout the unique artist, we'll loop based on it 
    uniq_artist = artist_info_selected %>% 
      ungroup() %>%
      select(X1) %>%
      distinct
    artists = uniq_artist[['X1']]
    
    # skip artisting who found cluster in last batches
    artistNoCluster_i = artistNoCluster %>% 
      filter(yr == i) %>%
      ungroup() 

    # loop through every artist
    for (artist in artists){
      if(artist %in% artistNoCluster_i$input_artist){
        result <- loopingForCluster(artist, samegroup_list_test, selected_artists_test, top_n_conv=top_n_conv, randomPick=randomPick, 
                                  arrangeMetric=arrangeMetric, biTop=biTop, artist_info=artist_info_selected, loopStop=loopStop,
                                  loopStopNum=loopStopNum)
        output = artist_name %>%
        filter(artist_id %in% (result[['samegroup_list']] %>% str_remove("a_")))
        output["input_artist"] = artist
        output['yr'] = i
        concat_output_allArtists = rbind(concat_output_allArtists, output)
      }
    }
  }
  
  # post process
  concat_output_allArtists_process = concat_output_allArtists %>%
    left_join(artist_name_df, by=c("input_artist"="clusterID")) %>%
    select(-artist_id.y, -num_core_fan) %>% 
    mutate(artist_id.x = paste0("a_", artist_id.x)) %>%
    rename(output_artist=artist_id.x, 
           output_artist_name=artist_name.x,
           input_artist_name=artist_name.y
           )
  return(concat_output_allArtists_process)
}

```

# 3. Iterative loop
after loop we find out some artists don't have cluster, we use a relaxed parameter to run again. 
From top3 to top15, once the output cluster of artist exceed to minimum boundary, we adopt this cluster and exclude it from next loop.
This ensure stabler number of member in a cluster.

To tune the number of cluster, one can set a higher boundary
maximum number to end the loop is also flexible, i set 15 because i don't think over top15 2 artists are relevant enough.
```{r}
concat_output_allArtists_process_list = list()
artistNoCluster = artist_info %>% select(X1, yr) %>% distinct() %>% rename(input_artist=X1)
boundary = 8

# Note: loop below are estimated to run at least 10~15 min locally.
for (i in 3:17){
  concat_output_allArtists_process = loopAllArtistFunc(artist_info = artist_info, top_n_conv = i, artistNoCluster = artistNoCluster)
  # aggregate to gain number of cluster member by year and artist
  NclusterMem = concat_output_allArtists_process %>%
    group_by(yr, input_artist, input_artist_name) %>%
    summarize(NclusterMem=n()) %>%
    arrange(NclusterMem)
  print(paste0("loop", i, ' Max N cluster: ', NclusterMem$NclusterMem %>% max()))
  
  # generate list of next iteration
  artistNoCluster = NclusterMem %>% 
    filter(NclusterMem < boundary)
  
  # remove artist under #boundary members
  concat_output_allArtists_process_list[[i]] = NclusterMem %>% 
    filter(NclusterMem >= boundary) %>%
    left_join(concat_output_allArtists_process)
}
# include last batch of artists
concat_output_allArtists_process_list[[16]] = NclusterMem %>%
  filter(NclusterMem < boundary) %>%
  left_join(concat_output_allArtists_process)

# rbind all the dataframe
concat_output_allArtists_process_result = do.call(rbind, concat_output_allArtists_process_list)
concat_output_allArtists_process_result %>% select(yr, input_artist) %>% distinct

# test code
if((concat_output_allArtists_process_result %>% select(yr, input_artist) %>% distinct %>% length()) != 
   (artist_info %>% select(X1, yr) %>% distinct() %>% length())){
  warning("Some artists might not have cluster")
}

# Output 
write.csv(concat_output_allArtists_process_result, file=paste0("/Users/paulyu/project/concat_output_all_bound", boundary, ".csv"))
```
# 4. (Optional) Only one loop
```{r}
artistNoCluster = artist_info %>% select(X1, yr) %>% distinct() %>% rename(input_artist=X1)
concat_output_allArtists_1loop = loopAllArtistFunc(artist_info = artist_info, top_n_conv = top_n_conv, artistNoCluster = artistNoCluster)
```

# 5. result exploration
User can compare this result with 1.4 section (Sample single artist)
## density chart
```{r}
## highest growth artist in 17
concat_output_allArtists_process_result %>% filter(yr == 17, input_artist == 'a_5639529')

# density chart
concat_output_allArtists_process_result %>% select(yr, input_artist, NclusterMem) %>% distinct() %>%
  mutate(yr = yr %>% as.factor()) %>%
  ggplot(aes(x=NclusterMem, group=yr, fill=yr)) + 
    geom_density(adjust=1.5)
```

# 6. Grid search hyperparameter tuning
```{r}
concat_output = data.frame(artist_id=double(), artist_name=character(), num_core_fan=double(), random=logical(), top_N=double(), metric=character(), biTop=logical(), input_artist=character(), stringsAsFactors=FALSE)

artist_info = artist_info %>%
  select(X1, X2, "context.free.comb.uu.ratio", "weight.model") %>%
  arrange(-(!!as.name(arrangeMetric)))
arrangeMetrics = c("context.free.comb.uu.ratio", "weight.model")
randomPicks = c(T, F)
top_n_convs = c(2, 5)
biTops = c(T, F)
artists = c("a_2578468", "a_210761", "a_253716", "a_5603802", "a_531819")

for (arrangeMetric in arrangeMetrics){
  for (randomPick in randomPicks){
    for (top_n_conv in top_n_convs){
      for(biTop in biTops){
        for (artist in artists){
          selected_artists_test = c()
          samegroup_list_test = c()
          result <- loopingForCluster(artist, samegroup_list_test, selected_artists_test, top_n_conv=top_n_conv, randomPick=randomPick, arrangeMetric=arrangeMetric, biTop=biTop, artist_info=artist_info)
          output = artist_name %>%
            filter(artist_id %in% (result[['samegroup_list']] %>% str_remove("a_")) )
          output["input_artist"] = artist
          output["random"] = randomPick
          output["top_N"] = top_n_conv
          output["metric"] = arrangeMetric
          output["biTop"] = biTop
          concat_output = rbind(concat_output, output)
        }
        
      }
    }
  }
}
concat_output  = concat_output %>%
  left_join(artist_name_df, by=c("input_artist"="clusterID")) %>%
  select(-artist_id.y, -num_core_fan.x, -num_core_fan.y)
write.csv(concat_output, file="/Users/paulyu/project/concat_output_grid.csv")
```