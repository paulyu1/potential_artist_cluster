---
title: "dispersion metric"
author: "Paul Yu"
date: "7/3/2021"
output: html_document
---
# package
```{r}
library(tidyverse)
library(magrittr)
library(purrr)
```

# 0. Description
This file implements dispersion metric by calculating the embedding weighting average of absent this year-present last year artist.


# 1.Data input
## artist cluster data
```{r}
boundary = 8
artists_cluster = read.csv(paste0("concat_output_all_bound", boundary, ".csv")) %>%
  select(-X, NclusterMem)
```
## artist Name
```{r}
# read in 6 yrs artist name, core fan
yrs=c(17, 18, 19, 20, 21)
artist_name_a = data.frame()
for (yr in yrs){
  df = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yr, '-01-01.tsv'), col_names = TRUE) 
  artist_name_a = rbind(artist_name_a, df)
}
artist_name_a = artist_name_a %>% 
  select(-num_core_fan) %>%
  distinct() %>% 
  mutate(artist_id = paste0("a_", artist_id))
```
## embedding data 
```{r}
yrs=c(17, 18, 19, 20, 21)
embeddingData= data.frame()
for (yr in yrs){
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yr, '-01-01.tsv'), 
                                   sep="", header = F, skip = 1, col.names=c('artist', 'x', 'y'))
  df[['yr']] = yr
  embeddingData = rbind(embeddingData, df)
}
```

## growth data
```{r}
above_threshold_numCoreFan_5yrs = read.csv("above_threshold_numCoreFan_5yrs.csv") %>%
  select(-X)
```

# 2. Cluster dispersion
## evaluate growth consider cluster member change
1. description: 
    This function helps you trace artist present in last year but absent in this year.
    The main purpose of this is to see if a cluster disperse (the artist within disperse) or 
    cohere (the artist within cohere and depart from other clusters)
2. output: all available year
3. output also dependent on:  
           yrs
           absentMeasureStyle
4. 第一年可能會失準，應要從數據出現的第一年開始算(合理的exist=1)
5. 
只有一年出現的可能會特別高 ＝> 刪掉
如果1 -1absent or not加權的分母正負抵消會有什麼影響 ＝> 延伸版
還是直接用growth metric ＝> 不行
6. 
```{r}
findAbsentClusterMember = function(i){
  coverYear = targetArtistYr[[i]]
  targetArtist = names(targetArtistYr)[[i]]
  if (length(coverYear) == 1){
    return()
  }
  absentMember = artists_cluster %>% 
    filter(input_artist == targetArtist) %>%
    select(output_artist, yr) %>%
    group_by(yr) %>%
    group_split() %>% lapply(function(cluster){
      return(cluster$output_artist)
    })
  
  # compare cluster member difference
  if(absentMeasureStyle == "allPrevious"){
    # compare cluster member difference in all previous year
    absentMember = lapply(seq(1, length(absentMember)), function(x){
         setdiff(absentMember[1:x-1] %>% unlist(), absentMember[x])
      }) %>%
      extract(-1)
  } else if (absentMeasureStyle == "adjacent"){
    # compare cluster member difference in 2 adjacent year
    absentMember = lapply(seq(1, length(absentMember)-1), function(x){
        setdiff(absentMember[[x]] , absentMember[[x+1]])
      })
  }
  names(absentMember) <- coverYear[-1]
  absentMember = absentMember %>%
    map_dfr(~as.data.frame(.), .id="yr") %>%
    rename("output_artist"=".") %>%
    mutate(absent=T, input_artist=targetArtist)
  return(absentMember)
}
cluster_embedding_disperse_func = function(absentMeasureStyle = "adjacent"){
  # This argument is to toggle if the presence in 2 year (or more) ago are considered
  
  # generate all targeted artists
  targetArtistYr = artists_cluster %>%
    select(yr, input_artist) %>%
    distinct()
  targetArtistYr = split(targetArtistYr$yr, targetArtistYr$input_artist)
  
  # loop through all targeted artists and output absent member compared to cluster in previous year
  df_allAbsentMember = seq_along(targetArtistYr) %>%
    lapply(findAbsentClusterMember) %>% 
    bind_rows() %>%
    mutate(yr = yr %>% as.double())
  
  # merge output and input artist embedding coordinates to calculate distance
  cluster_embedding = artists_cluster %>% 
    select(-output_artist_name, -input_artist_name, -NclusterMem) %>% 
    mutate(absent = F) %>%
    rbind(df_allAbsentMember) %>%
    left_join(embeddingData, by=c("output_artist"="artist", 'yr'='yr')) %>%
    rename(output_x=x, output_y=y) %>%
    left_join(embeddingData, by=c("input_artist"="artist", 'yr'='yr')) %>%
    rename(input_x=x, input_y=y) %>%
    left_join(artists_cluster %>% select(-yr, -NclusterMem) %>% distinct(), 
              by=c('input_artist'='input_artist', 'output_artist'='output_artist')) %>% 
    mutate(dist=((output_x-input_x)^2 + (output_y-input_y)^2)^(1/2))
  
  # calculate weight dispersion value, which is also existence weighted by distance, and disperse_diff
  cluster_embedding_disperse = cluster_embedding %>%
    mutate(absent=ifelse(absent == T, 1, -1)) %>%
    group_by(input_artist, yr) %>%
    mutate(disperse=absent * dist) %>%
    summarize(sumDisperse=sum(disperse, na.rm = TRUE) / n(), absent_cnt=sum(absent == 1), input_artist_name=input_artist_name %>% unique()) %>%
    group_by(input_artist_name) %>%
    filter(n() > 1) %>% # remove artist existing for only one year
    left_join(artist_name_a %>% select(artist_id, artist_name) %>% distinct(), 
              by=c("input_artist"="artist_id")) %>%
    group_by(input_artist) %>%
    mutate(disperse_diff=sumDisperse - sumDisperse %>% lag()) %>%
    mutate(cohere=ifelse(disperse_diff > 0, F, T)) %>% #less disperse => cohere
    rename(yr_cluster=yr)
  
  # For new artist appear not in first year, we consider it as cohere, because only by cohesion one artist can be above the threshold and listed.
  cluster_embedding_disperse[(is.na(cluster_embedding_disperse$disperse_diff) & cluster_embedding_disperse$yr_cluster!=yrs[1]), 'cohere'] = T
  
  return(cluster_embedding_disperse)
}
cluster_embedding_disperse = data.frame(input_artist=character(), yr=double(), sumDisperse=double(), absent_cnt=integer(), 
                                        input_artist_name=character(), artist_name=character(), disperse_diff=double(), cohere=logical())
cluster_embedding_disperse = cluster_embedding_disperse_func()
```
Naming variable yr_cluster is to differentiate yr_GrowthMetric
you can also refer to growth metric.Rmd 4.1

ref
loop over lapply:
https://stackoverflow.com/questions/46555433/how-to-loop-over-a-sliding-window-of-data-with-lapply
tilde (~) and dot (.) operatior: 
https://stackoverflow.com/questions/59855095/tilde-and-dots-in-r

# 3. result exploration
negative: cohere
positive: disperse
(*pending, further researche needed) disperse_diff ideally should represent
## general test
```{r}
# select cirtain artist and compare with artist cluster data
cluster_embedding_disperse %>%
  filter(input_artist == "a_5603802")
artists_cluster %>% 
  filter(input_artist == 'a_5603802', yr_cluster %in% c(18, 19))

# arrange cluster of more cohesion
cluster_embedding_disperse %>%
  filter(yr_cluster == 18) %>%
  arrange(-sumDisperse)

# check dispersion metric distribution and mean to justify universal selected threshold to compare different period
cluster_embedding_disperse %>% 
  mutate(yr_cluster=as.factor(yr_cluster)) %>%
  ggplot(aes(x=sumDisperse, group=yr_cluster, fill=yr_cluster)) + 
    geom_density(adjust=1.5)
```
## sample smaller group
```{r}
## 直男樂團
cluster_embedding_disperse %>%
  filter(input_artist %in% c("a_5603802", "a_847562", "a_531819", "a_968339", "a_350158", "a_531819")) %>%
  select(-input_artist_name)

## 小清新
cluster_embedding_disperse %>%
  filter(input_artist %in% c("a_193606", "a_29702", "a_2307695", "a_1220933", "a_531096")) %>%
  select(-input_artist_name)

## 嘻哈
cluster_embedding_disperse %>%
  filter(input_artist %in% c("a_18158", "a_4058209", "a_247797", "a_7332", "a_3461098", "a_43930")) %>%
  select(-input_artist_name)
```

# 4. combine dispersion + growth metric

```{r}
# find every year top based on num core fan
cluster_embedding_disperse %>%
  filter(yr_cluster != 17) %>% #ignore 1st year for we don't detect absence from last year 
  group_by(yr_cluster) %>%
  top_n(100, -disperse_diff) %>%
  arrange(yr_cluster, sumDisperse) %>%
  select(-input_artist_name) %>%
  inner_join(above_threshold_numCoreFan_5yrs, by=c('input_artist'='input_artist', 'yr_cluster'='yr_cluster')) %>%
  arrange(-growth) %>%
  distinct()
```
(pending) yr_cluster配yr_cluster定義有點模糊
因為yr_cluster是一個會變動的群，如果配上above_threshold_numCoreFan_5yrs(growth)的yr_numcorefan會不知道比較基準是什麼（yr_numcorefan會與yr_cluster有關）

## 直男
```{r}
cluster_embedding_disperse %>%
  left_join(coreMemVol %>% filter(yr_cluster == 18), by=c("input_artist"="input_artist", "yr"="yr_numCoreFan")) %>%
  select(-input_artist_name.x, -input_artist_name.y) %>%
  filter(yr_cluster %in% c(17, 18, 19)) %>%
  filter(input_artist %in% c("a_5603802", "a_847562", "a_531819", "a_968339", "a_350158", "a_531819"))
```
## 小清新
```{r}
cluster_embedding_disperse %>%
  left_join(coreMemVol %>% filter(yr_cluster == 18), by=c("input_artist"="input_artist", "yr"="yr_numCoreFan")) %>%
  select(-input_artist_name.x, -input_artist_name.y) %>%
  filter(yr_cluster %in% c(17, 18, 19)) %>%
  filter(input_artist %in% c("a_193606", "a_29702", "a_2307695", "a_1220933", "a_531096"))
```