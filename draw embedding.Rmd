---
title: "draw embedding"
author: "Paul Yu"
date: "2021/5/4"
output: html_document
---
#package
```{r}
library(tidyverse)
library(fuzzyjoin)
```
# 0. Description and abstract
This file is exploratory data analysis of artist embedding data.
We want to probe a. if there's a potential group b. if this potential group grow over year in terms of certain metric.

# 1. Data input artist data
```{r}
artist <- read_tsv(file='dta/artist_info/artist_core_fan_size_2017-01-01.tsv', col_names = TRUE)
artist %>% filter(grepl('董事長', artist_name))
```
# 2. artist based
畫出不同年份的相似樂團來看
## indie - 直男樂團
```{r}
df <- read.table(file='dta/embedding_result/representive_C2_line1_2019-01-01.tsv', sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
df_fil = df %>% filter(grepl('^a_74$|^a_74/|/a_74$', arti)) #user and artist

scat_embedding = df %>%
  # 草東 老王 美秀
  filter('a_5603802' == arti | 'a_531819' == arti  | 'a_9836586' == arti) %>% 
  ggplot(aes(x=x, y=y)) +
  geom_point(shape=1) +
  lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
scat_embedding

read.table(file='dta/embedding_result/representive_C2_line1_2020-01-01.tsv', sep=" ", skip=1, col.names = c('arti', 'x', 'y')) %>% 
  filter('a_5603802' == arti | 'a_531819' == arti  | 'a_9836586' == arti)

# 
# 2019 草東出現
# 2020 老王 告五 出現 
# 2021 繼續出現也一起移動
# 
```

## indie - 小清新
```{r}
df1 <- read.table(file='dta/embedding_result/representive_C2_line1_2021-01-01.tsv', sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
scat_embedding1 = df1 %>%
  #原子邦妮 脆樂團 Hello Nico 理想混蛋
  filter('a_210761' == arti | 'a_253716' == arti  | 'a_2307695' == arti  | 'a_10616389' == arti) %>% 
  ggplot(aes(x=x, y=y)) +
  geom_point(shape=1) +
  lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y))) +
  geom_text(aes(label=arti), hjust=0, vjust=0)
scat_embedding1

# 2017 HelloNico
# 2018 HelloNico 原子
# 2019 HelloNico 原子 理想混蛋
# 2020 原子 理想混蛋 (Hello Nico沒出新歌)
# 2021 原子 脆樂團 理想混蛋
```

## mixed
```{r}
yr=21
artist <- read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yr, '-01-01.tsv'), col_names = TRUE)
df2 <- read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yr, '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
artist = artist %>% mutate(artist_id = paste0('a_', artist_id))
df2 = df2 %>% left_join(artist, by=c("arti" = "artist_id"))
df2 = df2 %>% mutate(tag = case_when(
                              'a_210761' == arti | 'a_253716' == arti  | 'a_2307695' == arti  | 'a_10616389' == arti ~ "fresh",
                              'a_5603802' == arti | 'a_531819' == arti  | 'a_9836586' == arti ~ "straight",
                              TRUE ~ "other"))
scat_embedding2 = df2 %>%
  filter(tag != "other") %>% 
  ggplot(aes(x=x, y=y, col=tag)) +
  geom_point(shape=1) +
  lims(x=c(min(df2$x), max(df2$x)), y=c(min(df2$y), max(df2$y))) +
  geom_text(aes(label=arti), hjust=0, vjust=0)
scat_embedding2
print(df2 %>% filter(tag != "other") )


# 模糊地帶
# 2018: hello nico
# 2019: 原子邦妮 （ＨＮ反而遠離）
# 2020: 原子邦尼
# 2021: 原子邦尼
#
# 整體切割算清楚
# 原子邦尼邏輯在於他是電子音樂比較不是純粹樂團編制的小清新
```
## hiphop - 台客風格 party 
```{r}
yr = 21
df1 <- read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yr, '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
scat_embedding1 = df1 %>%
  # 頑童 MChotdog 兄弟本色 Maztka 黃明志
  filter('a_2578468' == arti | 'a_7332' == arti | 'a_4058209' == arti) %>% 
  ggplot(aes(x=x, y=y)) +
  geom_point(shape=1) +
  lims(x=c(min(df1$x), max(df1$x)), y=c(min(df1$y), max(df1$y))) +
  geom_text(aes(label=arti), hjust=0, vjust=0)
scat_embedding1

# 張震嶽本身離這個最遠
# 2019 頑童 脫離其他的
# 2020 頑童 脫離更開
# 2020 頑童 還是脫離但是有靠近
```
## hiphop - pop
```{r}
yr = 21
df1 <- read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yr, '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
artist <- read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yr, '-01-01.tsv'), col_names = TRUE)
artist = artist %>% mutate(artist_id = paste0('a_', artist_id))
df1 = df1 %>% left_join(artist, by=c("arti" = "artist_id"))
df1
scat_embedding1 = df1 %>%
  # 婁峻碩 高爾宣 呂士軒 Ｌeo 王 OZI 熊仔 熊仔豹子膽
  filter('a_9792' == arti | 'a_12719146' == arti | 'a_7342543' == arti | 'a_3342402' == arti | 'a_10014362' == arti |'a_3461098' == arti  | 'a_114108884' == arti ) %>% 
  ggplot(aes(x=x, y=y)) +
  geom_point(shape=1) +
  geom_text(aes(label=arti), hjust=0, vjust=0) +
  lims(x=c(min(df1$x), max(df1$x)), y=c(min(df1$y), max(df1$y)))
scat_embedding1
print(df1 %>% filter('a_9792' == arti | 'a_12719146' == arti | 'a_7342543' == arti | 'a_3342402' == arti | 'a_10014362' == arti |'a_3461098' == arti  | 'a_114108884' == arti))

# 2019 Leo Ozi 呂 熊 同時出現 
# 2020 婁俊碩 高爾宣（微微脫離）
# 2021 Leo王 比較脫離
#
# 整體來說比起搖滾這個距離比較不會差那麼多
```

## mixed
```{r}
yr=21
artist <- read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yr, '-01-01.tsv'), col_names = TRUE)
df2 <- read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yr, '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
artist = artist %>% mutate(artist_id = paste0('a_', artist_id))
df2 = df2 %>% left_join(artist, by=c("arti" = "artist_id"))
df2 = df2 %>% mutate(tag = case_when(
  # 頑童 MChotdog 兄弟本色 Maztka 黃明志 謝金燕 伍佰
  # 婁峻碩 高爾宣 呂士軒 Ｌeo 王 OZI 熊仔 熊仔豹子膽 Julia
                              'a_2578468' == arti | 'a_7332' == arti | 'a_4058209' == arti | 'a_3095648' == arti | 'a_467924' == arti | 'a_6281' == arti | 'a_6655' == arti ~ "tw_style",
                              'a_9792' == arti | 'a_12719146' == arti | 'a_7342543' == arti | 'a_3342402' == arti | 'a_10014362' == arti |'a_3461098' == arti | 'a_114108884' == arti | 'a_2949046' == arti ~ "pop",
                              TRUE ~ "other"))
scat_embedding2 = df2 %>%
  filter(tag != "other") %>% 
  ggplot(aes(x=x, y=y, col=tag)) +
  geom_point(shape=1) +
  lims(x=c(min(df2$x), max(df2$x)), y=c(min(df2$y), max(df2$y))) +
  geom_text(aes(label=arti), hjust=0, vjust=0)
scat_embedding2
print(df2 %>% filter(tag != "other") )

# 2017 還只有台客的
# 2019 pop 類型出現 但會重疊
# 2020 911脫離 其他台客繼續跟 pop分離
# 2021 911 較沒有這麼脫離
#
# 整體切割不明顯 有可能是因為我選pop？ 比較有重疊可能（不管任何人都會聽）
#
# 其他台客加入 相距很遠
```
# 3. user based 
畫出user為主的相似樂團移動
## function
```{r}
# common function: this function is to see the core fan embedding by grepping the artist id in the all embedding vector
mergeCoreFanNArtistName = function(arg_artist_id, df_emb){
    df1 = df_emb %>% 
      filter(grepl(paste0("[/]", arg_artist_id, "|", arg_artist_id, "[/]"), arti)) %>% 
      separate(col = arti, into = c("left", "right"), sep = "/") %>% 
      gather(key=leftright, value=artist_id, left:right) %>%
      select(-leftright)
    df1 = df1 %>% 
      filter(artist_id == arg_artist_id) %>%
      left_join(artists, by="artist_id")
    return(df1)
}
```

## hiphop - pop
```{r}
yrs=c(17, 18, 19, 20, 21)
#婁峻碩 高爾宣 呂士軒 Ｌeo 王 OZI 熊仔 熊仔豹子膽
arg_artist_id = c("a_9792", "a_12719146", "a_7342543", "a_3342402", "a_10014362", "a_3461098", "a_114108884")
  
for(i in 1:length(yrs)){
  artists <- read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df <- read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  
  
  
  df_concat = do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb=df)) %>% 
    mutate(tag = case_when(
    # 婁峻碩 高爾宣 呂士軒 Ｌeo 王 OZI 熊仔 熊仔豹子膽
                                "婁峻碩" == artist_name ~ "LoChungShuo",
                                "高爾宣 OSN" == artist_name ~ "KaoErShiuan",
                                "呂士軒 (TroutFresh)" == artist_name ~ "LuShiShiuan",
                                "Leo王" == artist_name ~ "Leo Wang",
                                "ØZI" == artist_name ~ "OZI",
                                "熊仔" == artist_name ~ "ShongZai",
                                "豹子膽" == artist_name ~ "BaoZhiDan",
                                TRUE ~ "other"))
  print(df_concat %>%
      # 婁峻碩 高爾宣 呂士軒 Leo 王 OZI 熊仔 熊仔豹子膽
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(shape=1) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
```

## hiphop - 台客
```{r}
yrs=c(17, 18, 19, 20, 21)

arg_artist_id = c('a_2578468', 'a_7332', 'a_4058209', 'a_18158')
for(i in 1:length(yrs)){
  artists = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  # 玖壹壹 MChotdog 兄弟本色 頑童
  df_concat <- do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb=df)) %>% 
    mutate(tag = case_when(
                                "兄弟本色G.U.T.S. (姚中仁、張震嶽、頑童MJ116)" == artist_name ~ "GUTs",
                                "MC HotDog熱狗" == artist_name ~ "HotDog",
                                "玖壹壹" == artist_name ~ "911",
                                "頑童MJ116" == artist_name ~ "naughty boy MJ116",
                                TRUE ~ "other"))
  print(df_concat %>%
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(shape=1) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
```
## Mixed Hiphop
```{r}
yrs=c(17, 18, 19, 20, 21)

arg_artist_id = c('a_2578468', 'a_7332', 'a_4058209', 'a_18158', "a_9792", "a_12719146", "a_7342543", "a_3342402", "a_10014362", "a_3461098", "a_114108884")
for(i in 1:length(yrs)){
  artists = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  # 玖壹壹 MChotdog 兄弟本色 頑童 婁峻碩 高爾宣 呂士軒 Ｌeo 王 OZI 熊仔 熊仔豹子膽
  df_concat <- do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb = df)) %>% 
    mutate(tag = case_when(
                                "兄弟本色G.U.T.S. (姚中仁、張震嶽、頑童MJ116)" == artist_name ~ "Taike",
                                "MC HotDog熱狗" == artist_name ~ "Taike",
                                "玖壹壹" == artist_name ~ "Taike",
                                "頑童MJ116" == artist_name ~ "Taike",
                                "婁峻碩" == artist_name ~ "Pop hiphop",
                                "高爾宣 OSN" == artist_name ~ "Pop hiphop",
                                "呂士軒 (TroutFresh)" == artist_name ~ "Pop hiphop",
                                "Leo王" == artist_name ~ "Pop hiphop",
                                "ØZI" == artist_name ~ "Pop hiphop",
                                "熊仔 (Kumachan)" == artist_name ~ "Pop hiphop",
                                "豹子膽" == artist_name ~ "Pop hiphop",
                                TRUE ~ "other"))
  print(df_concat %>%
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(shape=1) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
```
台客變潮？

## indie 小清新
```{r}
yrs=c(17, 18, 19, 20, 21)

arg_artist_id = c('a_210761', 'a_253716', 'a_2307695', 'a_10616389')
for(i in 1:length(yrs)){
  artists = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  
   #原子邦妮 脆樂團 Hello Nico 理想混蛋
  df_concat <- do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb=df)) %>% 
    mutate(tag = case_when(
                                "原子邦妮 (Astro Bunny)" == artist_name ~ "Bonny",
                                "Crispy脆樂團" == artist_name ~ "Tsui Band",
                                "Hello Nico" == artist_name ~ "Hello Nico",
                                "房東的貓 (The Landlord's Cat)" == artist_name ~ "Landlord Cat",
                                TRUE ~ "other"))
  print(df_concat %>%
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(shape=1) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
```
## indie - 直男樂團
```{r}
yrs=c(17, 18, 19, 20, 21)

arg_artist_id = c('a_5603802', 'a_531819', 'a_9836586')

for(i in 1:length(yrs)){
  artists = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  
   
  df_concat <- do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb=df)) %>% 
    mutate(tag = case_when(
                                "美秀集團 (Bisiu Group)" == artist_name ~ "Bisiu",
                                "老王樂隊 (Your Woman Sleep With Others)" == artist_name ~ "Wang",
                                "草東沒有派對 (No Party For Cao Dong)" == artist_name ~ "No party",
                                TRUE ~ "other"))
  print(df_concat %>%
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(shape=1) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
```

## Mixed - indie
```{r}
yrs=c(17, 18, 19, 20, 21)
scat_embeddings=c()

arg_artist_id = c('a_210761', 'a_253716', 'a_2307695', 'a_10616389', 'a_5603802', 'a_531819', 'a_9836586')
for(i in 1:length(yrs)){
  artists = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  
  df_concat <- do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb=df)) %>% 
    mutate(tag = case_when(
                                "原子邦妮 (Astro Bunny)" == artist_name ~ "chingshing",
                                "Crispy脆樂團" == artist_name ~ "chingshing",
                                "Hello Nico" == artist_name ~ "chingshing",
                                "房東的貓 (The Landlord's Cat)" == artist_name ~ "chingshing",
                                "美秀集團 (Bisiu Group)" == artist_name ~ "chihnan",
                                "老王樂隊 (Your Woman Sleep With Others)" == artist_name ~ "chihnan",
                                "草東沒有派對 (No Party For Cao Dong)" == artist_name ~ "chihnan",
                                TRUE ~ "other"))
  print(df_concat %>%
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(shape=1) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
```

# 4. Other feature added in
## artist.score
artist score normalize in population.
```{r}
read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
```

## hippop-台客
```{r}
yrs=c(17, 18, 19, 20, 21)
arg_artist_id = c('a_2578468', 'a_7332', 'a_4058209', 'a_18158')
selected_indicator = "artist.score"

for(i in 1:length(yrs)){
  artists = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  artist_info = read.table(file=paste0('dta/network_links/representative_C2_03_info_trimmed_network_20', yrs[i], '-01-01.tsv'), sep="\t", header = TRUE) %>%
    select("X1", selected_indicator) %>%
    distinct() %>% 
    mutate_at(c(selected_indicator), ~(scale(.) %>% as.vector %>% round))

  # 玖壹壹 MChotdog 兄弟本色 頑童
  df_concat <- do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb=df)) %>% 
    mutate(tag = case_when(
                                "兄弟本色G.U.T.S. (姚中仁、張震嶽、頑童MJ116)" == artist_name ~ "GUTs",
                                "MC HotDog熱狗" == artist_name ~ "HotDog",
                                "玖壹壹" == artist_name ~ "911",
                                "頑童MJ116" == artist_name ~ "naughty boy MJ116",
                                TRUE ~ "other")) %>%
    left_join(artist_info, by=c("artist_id"="X1"))
    
  print(df_concat %>%
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(aes_string(size=selected_indicator)) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
# aes_string ref: https://stackoverflow.com/questions/22309285/how-to-use-a-variable-to-specify-column-name-in-ggplot
```
## Mixed Hippop
```{r}
yrs=c(17, 18, 19, 20, 21)
arg_artist_id = c('a_2578468', 'a_7332', 'a_4058209', 'a_18158', "a_9792", "a_12719146", "a_7342543", "a_3342402", "a_10014362", "a_3461098", "a_114108884")
selected_indicator = "artist.score"

for(i in 1:length(yrs)){
  artists = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  artist_info = read.table(file=paste0('dta/network_links/representative_C2_03_info_trimmed_network_20', yrs[i], '-01-01.tsv'), sep="\t", header = TRUE) %>%
    select("X1", selected_indicator) %>%
    distinct() %>% 
    mutate_at(c(selected_indicator), ~(scale(.) %>% as.vector %>% round))
  
  df_concat <- do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb=df)) %>% 
    mutate(tag = case_when(
                                "兄弟本色G.U.T.S. (姚中仁、張震嶽、頑童MJ116)" == artist_name ~ "Taike",
                                "MC HotDog熱狗" == artist_name ~ "Taike",
                                "玖壹壹" == artist_name ~ "Taike",
                                "頑童MJ116" == artist_name ~ "Taike",
                                "婁峻碩" == artist_name ~ "Pop hiphop",
                                "高爾宣 OSN" == artist_name ~ "Pop hiphop",
                                "呂士軒 (TroutFresh)" == artist_name ~ "Pop hiphop",
                                "Leo王" == artist_name ~ "Pop hiphop",
                                "ØZI" == artist_name ~ "Pop hiphop",
                                "熊仔 (Kumachan)" == artist_name ~ "Pop hiphop",
                                "豹子膽" == artist_name ~ "Pop hiphop",
                                TRUE ~ "other")) %>%
    left_join(artist_info, by=c("artist_id"="X1"))
    
  print(df_concat %>%
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(aes_string(size=selected_indicator)) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
```
## Mixed Indie
```{r}
yrs=c(17, 18, 19, 20, 21)
arg_artist_id = c('a_210761', 'a_253716', 'a_2307695', 'a_10616389', 'a_5603802', 'a_531819', 'a_9836586')
selected_indicator = "artist.score"

for(i in 1:length(yrs)){
  artists = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yrs[i], '-01-01.tsv'), col_names = TRUE)
  df = read.table(file=paste0('dta/embedding_result/representive_C2_line1_20', yrs[i], '-01-01.tsv'), sep=" ", skip=1, col.names = c('arti', 'x', 'y'))
  artists = artists %>% mutate(artist_id = paste0('a_', artist_id))
  artist_info = read.table(file=paste0('dta/network_links/representative_C2_03_info_trimmed_network_20', yrs[i], '-01-01.tsv'), sep="\t", header = TRUE) %>%
    select("X1", selected_indicator) %>%
    distinct() %>% 
    mutate_at(c(selected_indicator), ~(scale(.) %>% as.vector %>% round))
  
  df_concat <- do.call("rbind", lapply(arg_artist_id, mergeCoreFanNArtistName, df_emb=df)) %>% 
    mutate(tag = case_when(
                                "原子邦妮 (Astro Bunny)" == artist_name ~ "chingshing",
                                "Crispy脆樂團" == artist_name ~ "chingshing",
                                "Hello Nico" == artist_name ~ "chingshing",
                                "房東的貓 (The Landlord's Cat)" == artist_name ~ "chingshing",
                                "美秀集團 (Bisiu Group)" == artist_name ~ "chihnan",
                                "老王樂隊 (Your Woman Sleep With Others)" == artist_name ~ "chihnan",
                                "草東沒有派對 (No Party For Cao Dong)" == artist_name ~ "chihnan",
                                TRUE ~ "other")) %>%
    left_join(artist_info, by=c("artist_id"="X1"))
    
  print(df_concat %>%
      ggplot(aes(x=x, y=y, color=tag)) +
      geom_point(aes_string(size=selected_indicator)) +
      lims(x=c(min(df$x), max(df$x)), y=c(min(df$y), max(df$y)))
  )
}
```


