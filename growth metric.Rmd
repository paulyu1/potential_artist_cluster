---
title: "growth metric"
author: "Paul Yu"
date: "6/8/2021"
output: html_document
---
# pachage
```{r}
library(tidyverse)
library(geosphere)
library(magrittr)
library(purrr)
```

# 0. Description
This file combine 2 growth metric (left to be added more) with the cluster data.
a. num core fan b. complete count
after this, i also calculate the threshold of the growth by manually selecting a group of artist whom i think as an ideal representation of growth.
In the last part, debug section is archived. 1 major bug regarding repetitive member of different cluster is temporarily resolved.

# 1. Raw data Processing
you can get it from looping all the artist in the clustering.rmd 
## input artist cluster data
```{r}
boundary = 8
artists_cluster = read.csv(paste0("concat_output_all_bound", boundary, ".csv")) %>%
  select(-X, NclusterMem)
```
## input artist_name 
better measurement of core fan volume
這個只是我們需要artist名字 core fan是下一個比較精準
```{r}
# read in 6 yrs artist name, core fan
yrs=c(17, 18, 19, 20, 21)
artist_name_a = data.frame()
for (yr in yrs){
  df = read_tsv(file=paste0('dta/artist_info/artist_core_fan_size_20', yr, '-01-01.tsv'), col_names = TRUE)  
  artist_name_a = rbind(artist_name_a, df)
}
artist_name_a = artist_name_a %>% 
  select(-num_core_fan) %>%
  distinct() %>% 
  mutate(artist_id = paste0("a_", artist_id))
```
原本的num_core_fan有點問題，使用下面的user to comb data
*** 這部分的code跑過一次就可以了

## ***User to comb (Run once, input directly in 2.1)
```{r}
yrs=c(17, 18, 19, 20, 21)
user_to_comb_6yrs = data_frame(artist_id=character(), num_core_fan=integer(), yr=integer())
for (yr in yrs){
  user_to_comb = read.table(file=paste0('dta/user_to_comb/representative_C2_01_user_to_comb_20', yr, '-01-01.tsv'), sep="\t", header = TRUE)
  # separate comb e.g. X1/X2
  user_to_comb = user_to_comb %>%
  select(-X2) %>%
  separate(top.C2.artists.comb, c("X1", "X2"), "/")
  user_to_comb_ttl = user_to_comb %>% 
    group_by(X1, X2) %>%
    summarize(num_core_fan = n()) %>%
    ungroup()
  user_to_comb_ttl = rbind(user_to_comb_ttl %>% select(-X2), user_to_comb_ttl %>% select(-X1) %>% rename(X1 = X2))
  user_to_comb_ttl = user_to_comb_ttl %>% 
    group_by(X1) %>%
    summarize(num_core_fan = sum(num_core_fan)) %>%
    rename(artist_id = X1)
  user_to_comb_ttl['yr'] = yr
  user_to_comb_6yrs = rbind(user_to_comb_6yrs, user_to_comb_ttl)
}
user_to_comb_6yrs = user_to_comb_6yrs %>% 
  left_join(artist_name_a %>% select(artist_id, artist_name) %>% distinct())
user_to_comb_6yrs = user_to_comb_6yrs %>% distinct()
write.csv(user_to_comb_6yrs, file="/Users/paulyu/project/dta/user_to_comb/user_to_comb_6yrs.csv")
```
## ***cube (Run once, input directly in 2.1)
```{r}
library('DBI')
library("RPresto")
library("dplyr") 
```

```{r}
conn.presto <- RPresto::dbConnect(Presto(), catalog = 'hive', schema = 'default',
                                    user ="paulyu" ,
                                    host ="http://presto.rdc.kkinternal.com" , port = 18080)

PrestoQuery <- function(sql.statement, connection = conn.presto){
  ## SQL query to data.frame
  df <- dbFetch(dbSendQuery(conn = connection, sql.statement), -1) %>% as_tibble()
  return(df)
}

# example code
"select dt, count(*) pc, count(distinct msno) uu  from event_log where dt between '2020-09-01' and '2021-04-01' and event = 'Stream end' and object_type = 'episode' group by dt" %>%  PrestoQuery() -> podcast.dau.df

"describe ds_datacube_metering_artist_monthly" %>% PrestoQuery()
```
remember to include dt and condition. describe is a great tool to take a glance without pulling ton of data in.
```{r}
# main code
user_to_comb_6yrs$artist_id
  # WHERE ID IN (",  paste(user_to_comb_6yrs$artist_id, collapse = ", "), ")"

"select artist_id, complete_cnt, dt from ds_datacube_metering_artist_monthly where dt between '2016-01-01' and '2021-01-01' and terr_id = 0 AND member_type = 'Z' AND login_type_id = -1 AND age_grp = -1 AND gender = 'all'" %>% PrestoQuery() -> artists_complete_count

# test code
"select * from ds_datacube_metering_artist_monthly where dt between '2020-09-01' and '2020-10-02' and artist_id = 74 and terr_id = 0 AND member_type = 'Z' AND login_type_id = -1 AND age_grp = -1 AND gender = 'all'" %>% PrestoQuery() -> artists_metering_test

# processing format
artists_complete_count = artists_complete_count %>%
  mutate(dt = as.Date(dt), yr=format(dt,'%Y')) %>%
  arrange(dt)
artists_complete_count_2017_21 = artists_complete_count %>% 
  group_by(artist_id, yr) %>%
  summarize(complete_cnt = sum(complete_cnt))
artists_complete_count_2017_21 = artists_complete_count_2017_20 %>% 
  mutate(yr = str_sub(yr, 3, 4), artist_id = paste0("a_", artist_id)) %>% 
  mutate(yr = as.integer(yr)) %>% 
  mutate(yr = yr + 1)

write.csv(artists_complete_count_2017_21, file="/Users/paulyu/project/artists_complete_count.csv")
```
1. 一個月算一次metering
2. terr_id = 0 AND member_type = 'Z' AND login_type_id = -1 AND age_grp = -1 AND gender = 'all' 全都是分類，總共有1893種組合但是上面這個分類剛好是全部的
3. yr==21 means 20200101-20210101 配合num core fan的計算方法, num core fan 20210101就會被算21, 20200101 就會被算20

# 2. Data Input
cluster data is in section 1.1
```{r}
# input comb data
user_to_comb = read_csv("/Users/paulyu/project/dta/user_to_comb/user_to_comb_6yrs.csv")
user_to_comb = user_to_comb[,-1]

# input cube data
artists_complete_count = read_csv("/Users/paulyu/project/artists_complete_count.csv")
artists_complete_count = artists_complete_count[,-1]
```

# 3. number of member growth-total 
```{r}
# we could test c("a_2578468", "a_210761", "a_253716", "a_5603802", "a_531819", "a_74")
clusterMemNum = artists_cluster %>% 
  group_by(yr, input_artist) %>%
  summarize(total_cluster_member = n(), input_artist_name=input_artist_name %>% unique())

# how many cluster members
clusterMemNum %>%
  filter(input_artist == "a_5603802")
```

# 4. core member volume-total & average
整合num core fan
yr_cluster: 用哪一年的cluster來算growth metric
yr_numCoreFan: output artist那年的num core fan
所以可以yr_cluster=17 對上yr_numCoreFan= 18 19 20 => 用17年某input artist的cluster算這cluster的num core fan
```{r}
# total: combine 2 df by output artist, we want to calculate the cluster total volume
# avg: calculate ttl and avg num core fan
coreMemVol = artists_cluster %>% 
  left_join(user_to_comb, by=c("output_artist"="artist_id")) %>% 
  rename(yr_cluster=yr.x, yr_numCoreFan=yr.y) %>% 
  group_by(yr_cluster, yr_numCoreFan, input_artist) %>%
  summarize(ttl_num_core_fan = sum(num_core_fan), avg_num_core_fan = mean(num_core_fan), input_artist_name=unique(input_artist_name))
```

## sample smaller group
```{r}
# 2 yrs growth: 
  #直男樂團： 草東 麋先生 老王 VH TRASH 老王
coreMemVol %>% filter(input_artist == "a_5603802") #決定看草東哪一年
coreMemVol %>% #其他類似的也看
  filter(input_artist %in% c("a_5603802", "a_847562", "a_531819", "a_968339", "a_350158", "a_531819")) %>%
  filter(yr_cluster == 17) %>%
  arrange(input_artist)
  
  #小清新: Frande, waa wei, hello nico, 柯智棠, 告五人
coreMemVol %>% filter(input_artist == "a_193606") #決定看Fran哪一年
coreMemVol %>% filter(input_artist %in% c("a_193606", "a_29702", "a_2307695", "a_1220933", "a_531096")) %>%
  filter(yr_cluster == 19) %>%
  arrange(input_artist)

  #嘻哈: 頑童 兄弟本色 葛仲珊 MC Hotdog 熊仔 蛋堡
coreMemVol %>% filter(input_artist == "a_18158") #決定看頑童哪一年
coreMemVol %>% filter(input_artist %in% c("a_18158", "a_4058209", "a_247797", "a_7332", "a_3461098", "a_43930")) %>%
  filter(yr_cluster == 19) %>%
  arrange(input_artist)
```

# 5. complete count-total & average
a. 整合complete count
b. 
  yr_cluster: 用哪一年的cluster來算growth metric
  yr_completeCount: output artist那年的complete count
  所以可以yr_cluster=17 對上yr_completeCount= 18 19 20 => 用17年某input artist的cluster算這cluster的complete count
```{r}
# total: combine 2 df by output artist, we want to calculate the cluster total volume
# avg: calculate ttl and avg num core fan
clusterMem_completeCount = artists_cluster %>% 
  left_join(artists_complete_count, by=c("output_artist"="artist_id")) %>% 
  rename(yr_cluster=yr.x, yr_completeCount=yr.y) %>% 
  group_by(yr_cluster, yr_completeCount, input_artist) %>%
  summarize(ttl_complete_cnt = sum(complete_cnt), avg_complete_cnt = mean(complete_cnt), input_artist_name=unique(input_artist_name))
```
### sample smaller group
```{r}
# 2 yrs growth: 
  #直男樂團： 草東 麋先生 老王 VH TRASH 老王
clusterMem_completeCount %>% filter(input_artist == "a_5603802") #決定看草東哪一年
clusterMem_completeCount %>% #其他類似的也看
  filter(input_artist %in% c("a_5603802", "a_847562", "a_531819", "a_968339", "a_350158", "a_531819")) %>%
  filter(yr_cluster == 18) %>%
  arrange(input_artist)
  
  #小清新: Frande, waa wei, hello nico, 柯智棠, 告五人
clusterMem_completeCount %>% filter(input_artist == "a_193606") #決定看Fran哪一年
clusterMem_completeCount %>% filter(input_artist %in% c("a_193606", "a_29702", "a_2307695", "a_1220933", "a_531096")) %>%
  filter(yr_cluster == 19) %>%
  arrange(input_artist)

  #嘻哈: 頑童 兄弟本色 葛仲珊 MC Hotdog 熊仔 蛋堡
clusterMem_completeCount %>% filter(input_artist == "a_18158") #決定看頑童哪一年
clusterMem_completeCount %>% filter(input_artist %in% c("a_18158", "a_4058209", "a_247797", "a_7332", "a_3461098", "a_43930")) %>%
  filter(yr_cluster == 19) %>%
  arrange(input_artist)
```
# 6. threshold
1. This takes average of a group of growing artists (assigned manually) and get the threshold of so called "potential"
2. User can change artists chosen to be the threshold.
3. 當年直男樂團是特別高的，故直男樂團的0.85倍當作門檻，可調整
## num core fan
```{r}
# threshold
threshold_value_numCoreFan = coreMemVol %>% 
  filter(input_artist %in% c("a_5603802", "a_847562", "a_531819", "a_968339", "a_350158", "a_531819")) %>%
  filter(yr_cluster == 17) %>%
  arrange(input_artist) %>% 
  filter(yr_numCoreFan %in% c(17, 18)) %>%
  group_by(input_artist) %>%
  summarise(threshold_value = ttl_num_core_fan / ttl_num_core_fan %>% lag()) %>%
  ungroup() %>% 
  summarize(threshold_value = threshold_value %>% mean(na.rm=T)) %>% `$` (threshold_value)

# 17 18 all
above_threshold = coreMemVol %>% 
  filter(yr_cluster == 18 & yr_numCoreFan %in% c(17, 18)) %>%
  group_by(input_artist) %>%
  summarize(yr_cluster = unique(yr_cluster), growth = ttl_num_core_fan / ttl_num_core_fan %>% lag(), 
            input_artist = unique(input_artist), input_artist_name = unique(input_artist_name))
above_threshold %>% 
  filter(growth > threshold_value * 0.85) %>%
  arrange(growth %>% desc())

# 20 21 all
above_threshold = coreMemVol %>% 
  filter(yr_cluster == 20 & yr_numCoreFan %in% c(20, 21)) %>%
  group_by(input_artist) %>%
  summarize(yr_cluster = unique(yr_cluster), growth = ttl_num_core_fan / ttl_num_core_fan %>% lag(), 
            input_artist = unique(input_artist), input_artist_name = unique(input_artist_name))
above_threshold %>% 
  filter(growth > threshold_value * 0.85) %>% arrange(-growth)
```
## complete count-threshold
```{r}
# threshold
threshold_value = clusterMem_completeCount %>% #其他類似的也看
  filter(input_artist %in% c("a_5603802", "a_847562", "a_531819", "a_968339", "a_350158", "a_531819")) %>%
  filter(yr_cluster == 17) %>%
  arrange(input_artist) %>% 
  filter(yr_completeCount %in% c(17, 18)) %>%
  group_by(input_artist) %>%
  summarise(threshold_value = (ttl_complete_cnt + mean(ttl_complete_cnt)) / (ttl_complete_cnt %>% lag() + mean(ttl_complete_cnt))) %>%
  ungroup() %>% 
  summarize(threshold_value = threshold_value %>% mean(na.rm=T)) %>% `$` (threshold_value)

# 20 21 all
above_threshold = clusterMem_completeCount %>% 
  filter(yr_cluster == 20 & yr_completeCount %in% c(20, 21)) %>%
  arrange(input_artist, yr_completeCount) %>%
  group_by(input_artist) %>%
  summarize(yr_cluster = unique(yr_cluster), 
            growth = (ttl_complete_cnt / (ttl_complete_cnt %>% lag())), 
            nomi=ttl_complete_cnt, denom=ttl_complete_cnt %>% lag(),
            input_artist = unique(input_artist), input_artist_name = unique(input_artist_name))

clusterMem_completeCount %>%
  filter(input_artist %in% c('a_193606', 'a_531096'))
above_threshold %>% 
  filter(growth > threshold_value * 0.85) %>% arrange(-growth)
```

## output-numCoreFan
```{r}
# num core fan
above_threshold_numCoreFan_5yrs = data.frame(input_artist=character(), yr_cluster=double(), yr_numCoreFan=double(), growth=double(), 
                                             nomi=double(), denom=double(), input_artist_name=character())
for (yr in yrs){
  df = coreMemVol %>% 
    group_by(input_artist, yr_cluster) %>%
    arrange(input_artist, yr_numCoreFan) %>%
    summarize(yr_cluster = unique(yr_cluster), 
              yr_numCoreFan = unique(yr_numCoreFan),
              growth = ttl_num_core_fan / ttl_num_core_fan %>% lag(), 
              nomi=ttl_num_core_fan, denom=ttl_num_core_fan %>% lag(),
              input_artist = unique(input_artist), input_artist_name = unique(input_artist_name)) %>%
    filter(growth > threshold_value_numCoreFan * 0.85) %>% 
    arrange(-growth)
  above_threshold_numCoreFan_5yrs = rbind(df, above_threshold_numCoreFan_5yrs)
}
write.csv(above_threshold_numCoreFan_5yrs, file="/Users/paulyu/project/above_threshold_numCoreFan_5yrs.csv")
```


# 7. debug-Archive
## biTop false duplicate issue
description:
1. This calculation bases on the num core fan/complete count.
2. For yr_numCoreFan < yr_cluster group, duplicate is more likely to happen, because a later year cluster might include some new artist where previous year don't have therefore have 0 num core fan. The variance between artist cluster get less. 

log:
1. biTop=F, earlystop=3, top5, context free會有重複現象:
dupli 列出在每一年cluster numCoreFan下完全一樣的組合，大約20%=>重新修改clustering algorithm

2. biTop=T, earlystop=3, top3~15, context free, boundary=5 會有重複現象:
dupli 列出在每一年cluster numCoreFan下完全一樣的組合，大約23%=>原因來自於cluster比較小(受boundary影響)

3. biTop=T, earlystop=3, top3~15, context free, boundary=8 會有重複現象:
dupli 列出在每一年cluster numCoreFan下完全一樣的組合，大約在2.6%=>原因來自於cluster比較小(受boundary影響)
dupli 列出在每一年cluster numCoreFan下完全一樣的組合，在yr_cluster < yr_numCoreFan情況，大約10.6%

```{r}
dupli = data.frame('yr_cluster'=double(), 'yr_numCoreFan'=double(), 'duplicate'=double(), 'nrow'=double())
yrs=c(17, 18, 19, 20, 21)

for (i_yr_cluster in yrs){
  for (t_yr_numCoreFan in yrs) {
    nrow = coreMemVol %>%
        filter(yr_cluster==i_yr_cluster, yr_numCoreFan==t_yr_numCoreFan) %>%
        nrow()
    duplicate= coreMemVol %>%
        filter(yr_cluster==i_yr_cluster, yr_numCoreFan==t_yr_numCoreFan) %>%
        arrange(ttl_num_core_fan) %>%
        group_by(ttl_num_core_fan, avg_num_core_fan) %>%
        filter(n() > 1) %>%
        nrow()
    dupli = rbind(dupli, c(i_yr_cluster, t_yr_numCoreFan, duplicate, nrow))
  }
}
colnames(dupli) = c('yr_cluster', 'yr_numCoreFan', 'duplicate', 'nrow')
dupli = dupli %>% 
  mutate(dupli_proportion= duplicate / nrow)
# single year 
testSingleDuplicate = coreMemVol %>%
  filter(yr_cluster==21, yr_numCoreFan==18) %>%
  arrange(ttl_num_core_fan)
# average duplicate proportion across year
dupli$dupli_proportion %>% mean()
# average duplicate proportion across year for yr_cluster < yr_numCoreFan
dupli %>% filter(yr_cluster <= yr_numCoreFan) %>% `$` (dupli_proportion) %>% mean()
  
artists_cluster %>% filter(yr == 21, input_artist %in% c('a_16765543', 'a_3342402', 'a_7122548'))
test_5
```

